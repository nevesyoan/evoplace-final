<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'f1b68eac2be758fc918c4f1b82b22700bb226235',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'f1b68eac2be758fc918c4f1b82b22700bb226235',
            'dev_requirement' => false,
        ),
        'politsin/jquery-ui-touch-punch' => array(
            'pretty_version' => '1.0',
            'version' => '1.0.0.0',
            'type' => 'drupal-library',
            'install_path' => __DIR__ . '/../politsin/jquery-ui-touch-punch',
            'aliases' => array(),
            'reference' => '2fe375e05821e267f0f3c0e063197f5c406896dd',
            'dev_requirement' => false,
        ),
    ),
);
