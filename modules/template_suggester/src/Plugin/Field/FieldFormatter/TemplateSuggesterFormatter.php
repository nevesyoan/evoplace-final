<?php

namespace Drupal\template_suggester\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'template_suggester_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "template_suggester_formatter",
 *   label = @Translation("Template Suggester formatter"),
 *   field_types = {
 *     "template_suggester"
 *   }
 * )
 */
class TemplateSuggesterFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return [];
  }

}
